# Beginner Level Interview

- Pull the git to local environtment
- Composer Install & Composer Update
- Set env to your local using env.example
- Create simple migration, model,controller for student.
- Populate the table with student column contain id, name, course.
- Create function that linked to view 
- In view make form that required user to fill up name and student course (both text field).
- Create function to store the student to database.
- Push your code to the gitlab with your name as branch and your level


# Intermediate Level Interview

- Pull the git to local environtment
- Composer Install & Composer Update
- Set env to your local using env.example
- Create simple migration, model,controller for student.
- Populate the table with student column contain id, name, course.
- Create function that linked to view 
- In view make form that required user to fill up name and student course (both text field).
- Create function to store the student to database.
- Use library datatable to be able to list student data.
- Create action button in datatable to delete and edit student.
- Complete delete and edit function for student.
- Push your code to the gitlab with your name as branch and your level


# Advance Level Interview

- Pull the git to local environtment
- Composer Install & Composer Update
- Set env to your local using env.example
- Create simple migration, model,controller for student.
- Populate the table with student column contain id, name, course.
- Create function that linked to view 
- In view make form that required user to fill up name and student course (both text field).
- Create function to store the student to database.
- Use library datatable to be able to list student data.
- Create action button in datatable to delete and edit student.
- Complete delete and edit function for student.
- Create crud api with simple api key for student.
- Create postman documentation for the api.
- Provide postman documentation in git folder.
- Push your code to the gitlab with your name as branch and your level



# Sample of branch
-adib-beginner
